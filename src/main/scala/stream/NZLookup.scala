/*
 * slsp-deduplication
 * Copyright (C) 2023  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package stream

import org.apache.kafka.streams.{KafkaStreams, StoreQueryParameters}
import org.apache.kafka.streams.state.{QueryableStoreTypes, ReadOnlyKeyValueStore}


// with this we can process the deleted records
// this lookup can get the nz-id based on the iz-id
// this is stored in a local store so it won't work if slsp-deduplication has more than one replica
// for doc see
// Seymour, Mitch. Mastering Kafka Streams and ksqlDB. 1st edition. O’Reilly Media, Inc., 2021. Print.
// chapter 4, "Materialized Stores"
// https://swisscovery.slsp.ch/permalink/41SLSP_NETWORK/1ufb5t2/alma991170441479105501
class NZLookup(streams: KafkaStreams) {

  /**
   * Based on an IZ-ID like oai:alma.41SLSP_UBS:9972427470805504
   * it returns the NZ-ID like (EXLNZ-41SLSP_NETWORK)991170497303705501
   * @param izid
   * @return
   */
  def get(izid: String): String = {
    val iz_id_lookup: ReadOnlyKeyValueStore[String, String] = streams.store(
      StoreQueryParameters.fromNameAndType(
        "nzlookup",
        QueryableStoreTypes.keyValueStore[String, String]())
    )
    val result = iz_id_lookup.get(izid)
    result
  }

}
