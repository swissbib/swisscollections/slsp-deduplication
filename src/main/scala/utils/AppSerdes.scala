/*
 * slsp-deduplication
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package utils

import org.apache.kafka.common.serialization.Serdes.WrapperSerde
import org.apache.kafka.common.serialization.{Serde, Serdes}
import serde.{JsonDeserializer, JsonSerializer}
//import org.apache.kafka.streams.StreamsConfig
//import org.apache.kafka.streams.scala.serialization.Serdes

case class Author(login: String, id: Int, site_admin: Boolean)


object AppSerdes extends Serdes  {


  import serde.JsonSerializer._
  import serde.JsonDeserializer._

  sealed class AuthorSerde extends WrapperSerde[Author] (new JsonSerializer[Author],new JsonDeserializer[Author] )

  def AuthorSerdeWrapper: AuthorSerde = new AuthorSerde


  //StreamsConfig.BOOTSTRAP_SERVERS_CONFIG

}
