# SLSP Deduplication

This microservice clusters the documents from different Alma Institution Zones (delivered via the OAI-PMH interface of the various IZ) to one document, based on the network zone id.

It also take care of deletes (remove one document from a cluster of documents or fully remove a document) 

# Unit Tests

```
sbt run test
```

# Integration Tests

The only external system needed is a kafka cluster. You can do some integration testing manually. In `docker-compose.yml`, you can find a setup for a Kafka Cluster via docker-compose.

How to test the whole system.

Launch the kafka cluster and create the topics

```
make up
```

Produce some test data

```
./scripts/produce-test-data.sh
```

Set the following environment variables

```
APPLICATION_ID=slsp-deduplication
KAFKA_BOOTSTRAP_SERVERS=localhost:29092
TOPIC_DELETES=swisscollections-deletes
TOPIC_IN=swisscollections-filtered
TOPIC_OUT=swisscollections-deduplicated
KAFKA_STREAMS_STATE_DIR=/tmp
```

Run the slsp-deduplication program (click the green arrow in Main.scala in IntelliJ) or 
```
sbt run
```

Start a shell to consume the data of the deduplicated topic

``` 
./scripts/consume-deduplicated-topic.sh
```

You should have 2 records, the first one is the ZBS record, and the second one should be the ZBS record merged with the RZS record (cf. `records.txt`)

Start another shell to consume the data of the deletes topic

``` 
./scripts/consume-deletes-topic.sh
```

It should be empty.

Now send the first delete

``` 
./scripts/produce-test-delete1.sh
```

The deduplicated topic should have a new message which contains only the RZS record (wait ca. 10 seconds)

Now send the 2nd delete

``` 
./scripts/produce-test-delete2.sh
```

The filtered topic doesn't change and the deletes topic should have one message with the network id as a key (the value of the message doesn't matter)


Inspiration : https://github.com/mitch-seymour/mastering-kafka-streams-and-ksqldb/tree/master/chapter-04
