/*
 * slsp-deduplication
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package stream


import ch.memobase.settings.SettingsLoader
import org.apache.kafka.streams.{KafkaStreams, StoreQueryParameters}
import org.apache.kafka.streams.state.{QueryableStoreTypes, ReadOnlyKeyValueStore}
import org.apache.logging.log4j.scala.Logging

import scala.jdk.CollectionConverters._

object Main extends Logging{

  val topology = new KafkaTopology
  private val settings = new SettingsLoader(
    List().asJava,
    "app.yml",
    false,
    true,
    false,
    false
  )

  val streams = new KafkaStreams(
    topology.build(
      settings.getInputTopic,
      settings.getOutputTopic,
      settings.getProcessReportTopic,
      settings.getAppSettings
    ),
    settings.getKafkaStreamsSettings
  )
  val shutdownGracePeriodMs = 10000

  streams.start()

  val nzlookup = new NZLookup(streams)


  Runtime.getRuntime.addShutdownHook(new Thread(() => {
    logger.info("stream is close")
    streams.close()
  }))





  def main(args: Array[String]): Unit = {

  }

}
