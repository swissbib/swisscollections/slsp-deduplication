/*
 * slsp-deduplication
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package stream

import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.common.utils.Bytes
import org.apache.kafka.streams.kstream.{KTable, Materialized, Printed, ValueMapper, ValueMapperWithKey}
import org.apache.kafka.streams.state.{KeyValueStore, QueryableStoreTypes, ReadOnlyKeyValueStore}
import org.apache.kafka.streams.{StoreQueryParameters, StreamsBuilder, Topology}
import org.apache.logging.log4j.scala.Logging
import utils.MarcExtractorHelpers

import java.util.Properties

class KafkaTopology extends Logging {

  def build(
             topicIn: String,
             topicOut: String,
             reportingTopic: String,
             appSettings: Properties
           ): Topology = {

    val builder = new StreamsBuilder

    val source = builder.stream[String, String](topicIn)

    val Array(correctSlspId, noSlspId) = source
      .branch(
        //I'm going to check if all documents contain a SLSP netzone ID
        //should be true - but to be really sure - it's SLSP....
        (_, v) => MarcExtractorHelpers.hasSLSPNetZoneId(v), //let deletes go through as well
        (_, _) => true
      )
    //report the noSLSPId cases
    //use reporting system of memobase


    //A Materialized Kafka Store where we will store the relationships izid -> nzid
    val nzlookupMateriazlized: Materialized[String, String, KeyValueStore[Bytes, Array[Byte]]] =
      Materialized.as("nzlookup").withKeySerde(Serdes.String).withValueSerde(Serdes.String)

    //KTable where key is iz id and message nz id
    val kt1: KTable[String, String] = correctSlspId
      /*
      we store the relation iz id -> nz id to retrieve it for deletes
       */
      .mapValues(

        new ValueMapperWithKey[String, String, String] {
          override def apply(readOnlyKey: String, value: String): String =
            MarcExtractorHelpers.getNetworkZoneIdFromMessage(readOnlyKey, value)
        }

      )
      .groupByKey()
      .reduce((doc1: String, doc2: String) => {reduceToFirstDocument(doc1, doc2)}, nzlookupMateriazlized)




    //kt0 has as key the NZ id and as value the clustered record
    val kt0: KTable[String, String] = correctSlspId
      .mapValues(

        new ValueMapperWithKey[String, String, String] {
          override def apply(readOnlyKey: String, value: String): String = {
            MarcExtractorHelpers.preProcessRecord(readOnlyKey, value)
          }
        }

      )

      /* change keys
      network zone Id is going to be the document ID
      this enables us to reduce documents coming from several institutions zones
      but containing the same network zone Id
      the algorithm is quite simple because we are always dealing with the same types
      we want to reduce and we have simple String serdes
      it wouldn't be so easy if we want to run custom aggregations where the aggregated
      expression type is different from the source
      but of course - also possible
       */
      .groupBy(
        (key, value: String) => {
          MarcExtractorHelpers.getNetworkZoneIdFromMessage(key, value)
        }
        /*
        now we can reduce where necessary
        why error notification in intellij when mapValues is included??
         */
      ).reduce(
        (doc1, doc2) => {
          //by now simplified solution where I simply give back the value2 record
          //this "reducing" is a fantastic play around field for Silvia when exploring
          //the scala concepts. We need good and general merging algorithms
          //comparable to those in the CBS system - we could transfer the rules
          //implemented in CBS using the smart scala mechanisms
          val mergedRecord = MarcExtractorHelpers.mergeDocuments(doc1, doc2)
          if (mergedRecord.getBytes.length >=990000) {
            //if the merged record is too big, we don't merge
            doc1
          } else {
            mergedRecord
          }
        }
      )


    val Array(fullDeletes, updates) = kt0.toStream
      .branch(
        //We check if the result is a full delete or only an update (i.e. a smaller cluster of documents than before)
        (_, v) => MarcExtractorHelpers.isFullDelete(v), //let deletes go through as well
        (_, _) => true
      )


    //the updates are sent further down the data pipeline
    updates
      .filter((_,v) => v.getBytes.length <= 990000) //we don't send messages which are too big to kafka to avoid errors. Some very long records might disappear due to this filter, but this is not really probable
      .to(topicOut)
    //the full deletes are sent to the deletes topic and they will be deleted from solr
    fullDeletes.to("swisscollections-deletes")

    builder.build()

  }

  /**
   * Reduce two documents to the first one
   * A bit of a dummy method...
   * @param doc1
   * @param doc2
   * @return
   */
  def reduceToFirstDocument(doc1: String, doc2: String) : String = {
    doc1
  }
}
