/*
 * slsp-deduplication
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package serde

import org.apache.kafka.common.serialization.Serializer
import utils.Author



class JsonSerializer[T:  upickle.default.Writer] extends Serializer[T] {


  override def serialize(topic: String, data: T): Array[Byte] = {
    upickle.default.writeBinary[T](data)
  }
}

object JsonSerializer {
  implicit val authorW: upickle.default.Writer[Author] = upickle.default.macroW

}


