import Dependencies._


ThisBuild / scalaVersion := "2.13.15"
ThisBuild / organization := "ch.swisscollections"
ThisBuild / organizationName := "swisscollections"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  }
  else {
    None
  }
}


lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "slsp-deduplication",
    assemblyJarName in assembly := "app.jar",
    test in assembly := {},

    assemblyExcludedJars in assembly := {
      val cp = (fullClasspath in assembly).value
      cp filter { f =>
        //f.data.getName.contains("spark-core") ||
        f.data.getName == "jcl-over-slf4j-1.7.30.jar" ||
          f.data.getName == "commons-codec-1.13.jar"
      }
    },


    assemblyMergeStrategy in assembly := {
      case "log4j.properties" => MergeStrategy.first
      case "module-info.class" => MergeStrategy.discard
      case "log4j2.xml" => MergeStrategy.first
      //case "module-info.class" => MergeStrategy.first

      case PathList("META-INF", "versions", "9", "module-info.class") => MergeStrategy.first
      case PathList("org", "slf4j", "impl", "StaticLoggerBinder.class") => MergeStrategy.first
      case PathList("org", "slf4j", "impl", "StaticMDCBinder.class") => MergeStrategy.first
      case PathList("org", "slf4j", "impl", "StaticMarkerBinder.class") => MergeStrategy.first

/*
      case PathList("org", "apache", "commons", "logging", "impl", "SimpleLog.class") => MergeStrategy.first
      case PathList("org", "apache", "commons", "logging", "impl", "SimpleLog$1.class") => MergeStrategy.first
      case PathList("org", "apache", "commons", "logging", "impl", "NoOpLog.class") => MergeStrategy.first
      case PathList("org", "apache", "commons", "logging", "LogFactory.class") => MergeStrategy.first
      case PathList("org", "apache", "commons", "logging", "LogConfigurationException.class") => MergeStrategy.first
      case PathList("org", "apache", "commons", "logging", "Log.class") => MergeStrategy.last
*/
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)

    },

    mainClass in assembly := Some("stream.Main"),

    resolvers ++= Seq(
      "Memobase Utils" at "https://gitlab.switch.ch/api/v4/projects/1324/packages/maven"
    ),
    libraryDependencies ++= Seq(
      log4jApi,
      log4jCore,
      log4jSlf4j,
      log4jScala,
      kafkaClient,
      upickle,
      memobaseServiceUtils
        excludeAll(
        ExclusionRule(organization = "org.slf4j", name = "slf4j-api"),
        ExclusionRule(organization = "org.slf4j", name = "slf4j-log4j12"),
        ExclusionRule(organization = "org.slf4j", name = "jcl-over-slf4j"),
      ),
      scalaxml,
      scalatic,
      scalaTest % Test
    )
  )

