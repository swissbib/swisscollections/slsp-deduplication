/*
 * slsp-deduplication
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package utils

import stream.Main.nzlookup

import scala.xml.transform.{RewriteRule, RuleTransformer}
import scala.xml.{Elem, Node, NodeSeq, TopScope, XML}

object MarcExtractorHelpers {

  private val institutionPattern = """.*?:alma\.(.*?):.*$""".r
  private val idPattern = """.*?:alma\..*?:(.*)$""".r

  /**
   * Before sending the records to the deduplication, they are preprocessed
   * We add 035 and 986 fields, change the 001 field, and mark the iz specific
   * fields with an identifier in the $9 subfield
   *
   * @param key   the key of the kafka message
   * @param message the full oai record with header
   * @return
   */
  def preProcessRecord(key: String, message: String): String = {
    if (isDelete(message)) {
      // we let it as it is
      message
    } else {
      val record = extractMarcRecord(message)
      val enrichedRecord = add035and986WithInstitutionPrefixAndIdentifier(key, record)
      val result = <record>
        <metadata>
          {sortMarcRecord(markOrigin(replace001FieldByNzId(enrichedRecord)))}
        </metadata>
      </record>
      result.toString()
    }
  }


  /**
   * Get the network zone ID from the record (as string)
   * For delete messages which have only the IZ id in the header,
   * we do a lookup based on the related materialized store
   *
   * @param message (the marc record as text with oai header)
   * @return the network zone id
   */
  def getNetworkZoneIdFromMessage(key: String, message: String): String = {
    if(isDelete(message)) {
      nzlookup.get(key)
    }
    else {
      getNetworkZoneIdFromRecord(XML.loadString(message) \ "metadata" \ "record")
    }
  }


  /**
   * Get the network zone ID from the record (as xml)
   *
   * @param record the full oai record with header
   * @return the network zone id
   */
  def getNetworkZoneIdFromRecord(record: NodeSeq): String = {
    getSubfields(record, "035", "a")
      .filter(_.text.contains("EXLNZ-41SLSP_NETWORK")).head.text
  }

  /**
   * Merge doc1 and doc2.
   * Takes doc2 as a base for the Network fields (like 245), as the updates will
   * come via doc2
   *
   * Import the local fields (852, 949, ...)
   * from doc1, except the ones which are from the same institution
   * zone as doc2
   * The oai header is removed from the merged record
   *
   * This also takes care of delete messages and will remove a document from a
   * cluster if this is needed
   *
   * @param doc1 first document to aggregate (marc record with oai header, as text)
   *             (contains the already aggregated documents)
   * @param doc2 second document to aggregate (marc record with oai header, as text)
   * @return the aggregated document
   */
  def mergeDocuments(doc1: String, doc2: String): String = {
    if(isDelete(doc1)) {
      println("Delete received for a non-existing record, ignore")
      return doc2
    }

    val record1 = extractMarcRecord(doc1)

    if(isDelete(doc2)) {
      val deleted_id2 = getPrefixedDeletedId(doc2)
      //if doc2 is a delete, we only remove the corresponding fields
      val result = <record><metadata>{removeLocalFieldsFromIZ(deleted_id2, record1)}</metadata></record>
      //reorder the attributes to have tag, ind1 and ind2 in this order
      val idCluster = get001Field(result)
      println(s"""$deleted_id2 was deleted from $idCluster""")
      result.toString.replaceAll("ind2=\"([^\"]+)\" ind1=\"([^\"]+)\" tag=\"([^\"]+)\"", "tag=\"$3\" ind1=\"$2\" ind2=\"$1\"")
    } else {
      val record2 = extractMarcRecord(doc2)

      //note : id1 can contain a concatenation of multiple identifiers
      val id1 = getSubfields(record1, "986", "a").text
      val id2 = getSubfields(record2, "986", "a").text


      val record1WithoutLocalFieldsFromRecord2 = removeLocalFieldsFromIZ(id2, record1)

      //if doc2 is a delete don't add anything

      val fieldsToAdd = extractLocalFields(record1WithoutLocalFieldsFromRecord2)

      val verbBody: RewriteRule = new RewriteRule {
        override def transform(n: Node): Seq[Node] = n match {
          case elem: Elem if elem.label == "datafield" &&
            //we should insert 852 and 949 at another place than before 245
            elem.attribute("tag").head.text == "245" =>
            elem.prependedAll(
              fieldsToAdd
            )
          case n => n
        }
      }

      val mergedDoc = new RuleTransformer(verbBody).transform(record2)

      val mergedDocSorted = <record>
        <metadata>
          {sortMarcRecord(mergedDoc)}
        </metadata>
      </record>

      val idMerged = get001Field(mergedDocSorted)

      println(s"""$id1 and $id2 were merged to $idMerged""")

      //reorder the attributes to have tag, ind1 and ind2 in this order
      mergedDocSorted.toString.replaceAll("ind2=\"([^\"]+)\" ind1=\"([^\"]+)\" tag=\"([^\"]+)\"", "tag=\"$3\" ind1=\"$2\" ind2=\"$1\"")

    }

  }

  /**
   * Check if a record has a network zone id
   * or if it is a delete message
   *
   * @param message the full oai record with header
   * @return true/false
   */
  def hasSLSPNetZoneId(message: String): Boolean = {
    if(isDelete(message)){
      true
    } else {
      getSubfields(extractMarcRecord(message), "035", "a")
        .exists((node: Node) => {
          node.text.contains("EXLNZ-41SLSP_NETWORK")
        })
    }

  }

  /**
   * From an oai record, extract the marc record as an xml structure
   *
   * @param message the full oai record with header
   * @return the marc record as an xml structure
   */
  def extractMarcRecord(message: String): NodeSeq = {

    val record = XML.loadString(message.replace("xsi:schemaLocation=\"http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd\"", ""))
    val recordNoNamespace = removeNamespaces(record)
    val recordOnly = recordNoNamespace \ "metadata" \ "record"
    recordOnly
  }

  /**
   * Add a $9 subfield with origin information to all institution-specific fields
   *
   * @param record marc record
   * @return
   */
  def markOrigin(record: NodeSeq): NodeSeq = {
    val id = getSubfields(record, "986", "a").text.trim

    //to avoid too big messages in kafka, we only take the first 500 949 fields
    if (getFields(record, "949").size>500) {
        println("record "+id+" has more than five hundred 949 fields, the record will be shortened to the first 500")
      }
    val fieldsToAdd = addIzSourceToMarcField(
      getFields(record, "852")
        ++ getFields(record, "947")
        ++ getFields(record, "948")
        ++ getFields(record, "949").take(500)
        ++ getFields(record, "AVD")
        ++ get9XXLOCALFields(record),
      id
    )

    //a transformation rule
    val verbBody: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case elem: Elem if elem.label == "datafield" &&
          //we insert them arbitrarily before 245
          elem.attribute("tag").head.text == "245" =>
          elem.prependedAll(
            fieldsToAdd
          )
        case elem: Elem if elem.label == "datafield" &&
          elem.attribute("tag").head.text == "852" => NodeSeq.Empty

        case elem: Elem if elem.label == "datafield" &&
          elem.attribute("tag").head.text == "947" => NodeSeq.Empty

        case elem: Elem if elem.label == "datafield" &&
          elem.attribute("tag").head.text == "948" => NodeSeq.Empty

        case elem: Elem if elem.label == "datafield" &&
          elem.attribute("tag").head.text == "949" => NodeSeq.Empty

        case elem: Elem if elem.label == "datafield" &&
          elem.attribute("tag").head.text == "AVD" => NodeSeq.Empty

        case elem: Elem if elem.label == "datafield" &&
          is9XXLOCALField(elem) => NodeSeq.Empty

        //remove the oai header part of the record
        case elem: Elem if elem.label == "header" => NodeSeq.Empty
        case n => n
      }
    }

    val markedDoc = new RuleTransformer(verbBody).transform(record)

    markedDoc
  }


  /**
   * Extract the record id from the oai header
   * Already merged records don't have an identifier in the oai header
   *
   * @param oaiKey the oai identifier for example oai:alma.41SLSP_ZBS:9972338884305528
   * @return the prefix, for example 41SLSP_ZBS
   */
  def getInstitutionPrefixFromOaiKey(oaiKey: String): String =
    institutionPattern.findAllIn(oaiKey).group(1)

  /**
   * Extract the record id from the oai header
   * Already merged records don't have an identifier in the oai header
   *
   * @param oaiKey the oai identifier for example oai:alma.41SLSP_ZBS:9972338884305528
   * @return the id for example 9972338884305528
   */
  def getInstitutionIdFromOaiKey(oaiKey: String): String =
    idPattern.findAllIn(oaiKey).group(1)



  /**
   * Replace the 001 field by the Network Zone ID
   *
   * @param record
   * @return
   */
  def replace001FieldByNzId(record: NodeSeq): NodeSeq = {
    val networkzoneId = getNetworkZoneIdFromRecord(record).substring(22)

    val replace001: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case elem: Elem if elem.label == "controlfield" &&
          elem.attribute("tag").head.text == "001" =>
          <controlfield tag="001">{networkzoneId}</controlfield>
        case n => n
      }
    }
    new RuleTransformer(replace001).transform(record)
  }

  /**
   * Add a 035 and a 986 field to the record with the institution zone ID
   * and the institution prefix from the OAI header
   *
   * @param institutionOaiKey oai:alma.41SLSP_ZBS:9972338884305528
   * @param record            the full oai record
   * @return the full record with an added <datafield ind2=" " ind1=" " tag="986">
   *         <subfield code="a">(41SLSP_ZBS)9972338884305528</subfield>
   *         </datafield>
   */
  def add035and986WithInstitutionPrefixAndIdentifier(institutionOaiKey: String, record: NodeSeq): NodeSeq = {
    val prefixedinstitutionZoneId = s"""(${getInstitutionPrefixFromOaiKey(institutionOaiKey)})${get001Field(record)}"""

    val verbBody: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case elem: Elem if elem.label == "datafield" &&
          elem.attribute("tag").head.text == "245" =>

          elem.prependedAll(
            <datafield ind2=" " ind1=" " tag="035">
              <subfield code="a">{prefixedinstitutionZoneId}</subfield>
              <subfield code="9">{prefixedinstitutionZoneId}</subfield>
            </datafield>
              <datafield ind2=" " ind1=" " tag="986">
                <subfield code="a">{prefixedinstitutionZoneId}</subfield>
                <subfield code="9">{prefixedinstitutionZoneId}</subfield>
              </datafield>
          )
        case n => n
      }
    }
    new RuleTransformer(verbBody).transform(record)
  }


  def sortMarcRecord(marcRecord: NodeSeq): NodeSeq = {
    val record = <record>
      {marcRecord \\ "leader"}
      {marcRecord \\ "controlfield"}
      {(marcRecord \\ "datafield").sortWith(MarcExtractorHelpers.sortFieldsBasedOnTag(_, _))}
    </record>
    removeNamespaces(record)

  }

  def removeNamespaces(x: Node):Node = x match {
    case e:Elem => e.copy(scope=TopScope, child = e.child.map(removeNamespaces))
    case o => o
  }


  def sortFieldsBasedOnTag(node1: Node, node2: Node): Boolean = {
    val tag1 = (node1 \ "@tag").text
    val tag2 = (node2 \ "@tag").text
    tag1 < tag2
  }


  /**
   * Remove Local Fields from a record
   *
   * @param value the value of the source identifier, for example (41SLSP_ZBS)9972338884305528
   * @param record a full record
   * @return the record with the IZ specific fields removed
   */
  def removeLocalFieldsFromIZ(value: String, record: NodeSeq): NodeSeq = {
    val verbBody: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case elem: Elem if elem.label == "datafield" && isLocalFieldFromIZ(value, elem) => NodeSeq.Empty
        case n => n
      }
    }
    new RuleTransformer(verbBody).transform(record)
  }

  /**
   * Extract Local Fields from a specific IZ from a record
   *
   * @param value the value of the source identifier, for example (41SLSP_ZBS)9972338884305528
   * @param record a full record
   * @return the institution local fields
   */
  def extractLocalFieldsFromIZ(value: String, record: NodeSeq): NodeSeq = {
    val verbBody: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case elem: Elem if elem.label == "datafield" && isLocalFieldFromIZ(value, elem) => elem
        case elem: Elem if elem.label == "datafield" && !isLocalFieldFromIZ(value, elem) => NodeSeq.Empty
        case elem: Elem if elem.label == "controlfield" => NodeSeq.Empty
        case elem: Elem if elem.label == "leader" => NodeSeq.Empty
        case n => n
      }
    }
    new RuleTransformer(verbBody).transform(record)
  }

  /**
   * Extract Local Fields from a a record
   *
   * @param record a full record
   * @return the institution local fields
   */
  def extractLocalFields(record: NodeSeq): NodeSeq = {
    val verbBody: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case elem: Elem if elem.label == "datafield" && isLocalField(elem) => elem
        case elem: Elem if elem.label == "datafield" && !isLocalField(elem) => NodeSeq.Empty
        case elem: Elem if elem.label == "controlfield" => NodeSeq.Empty
        case elem: Elem if elem.label == "leader" => NodeSeq.Empty
        case n => n
      }
    }
    new RuleTransformer(verbBody).transform(record)
  }

  /**
   * Check if a marc field has a $9 subfield with the value in it
   *
   * @param value the value we search in $9
   * @param elem  the marc field
   * @return true if value is in $9, false otherwise
   */
  def isLocalFieldFromIZ(value: String, elem: Elem): Boolean = {
    getRSubfieldContent(elem)("9").contains(value)
  }


  /**
   * Check if a marc field has a $9 subfield with a value that starts with (41SLSP
   *
   * @param value the value we search in $9
   * @param elem  the marc field
   * @return true if value is in $9, false otherwise
   */
  def isLocalField(elem: Elem): Boolean = {
    val prefixToCheck = "(41SLSP_"
    //checks if one of the $9 field starts with (41SLSP
    getRSubfieldContent(elem)("9").filter(x => x.startsWith(prefixToCheck)).length > 0
  }

  /**
   * Check if the field has a tag starting with 9 and
   * if it has a $9 subfield with the value LOCAL
   *
   * @param node  the marc field
   * @return true/false
   */
  def is9XXLOCALField(node: Node): Boolean = {
    val prefixToCheck = "(41SLSP_"
    //checks if one of the $9 field starts with (41SLSP
    val tag = node.attribute("tag").head.text
    if (tag.startsWith("9")) {
      getRSubfieldContent(node)("9").map(x=>x.toLowerCase()).contains("local")
    } else {
      false
    }
  }

  /**
   * Gets content of a repeatable subfield
   *
   * @param field        field as XML `Node`
   * @param subfieldName subfield code
   * @return list of subfield values
   */
  protected def getRSubfieldContent(field: Node)(subfieldName: String): List[String] =
    (field \\ "subfield").filter(sf => sf \@ "code" == subfieldName).map(c => c.text).toList


  def get001Field(record: NodeSeq): String = {
    getAll001Fields(record).head.text.trim
  }

  //this is all very verbose and quickly implemented so it works -
  // make it more idiomatic - perhaps...
  def getAll001Fields(record: NodeSeq): NodeSeq = {
    for {
      cf <- record \\ "controlfield"
      if cf \@ "tag" == "001"
    } yield cf
  }



  def getFields(record: NodeSeq, field: String): NodeSeq = {
    for {
      df <- record \\ "datafield"
      if df \@ "tag" == field
    } yield df
  }

  def get9XXLOCALFields(record: NodeSeq): NodeSeq = {
    for {
      df <- record \\ "datafield"
      if is9XXLOCALField(df)
    } yield df
  }


  def getSubfields(record: NodeSeq, field: String, subfield: String): NodeSeq = {
    for {
      df <- record \\ "datafield"
      if df \@ "tag" == field
      sf <- df \\ "subfield"
      if sf \@ "code" == subfield
    } yield sf
  }


  /**
   *
   * @param fields   a marc field (or multiple marc fields)
   * @param sourceId the IZ id to add to the marc field (in $9)
   * @return the initial marc fields with the IZ id in $9
   */
  def addIzSourceToMarcField(fields: NodeSeq, sourceId: String): NodeSeq = {
    val verbBody: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case elem: Elem if elem.label == "datafield" =>
          elem.copy(child = (elem.child ++
            <subfield code="9">{sourceId}</subfield>
            ))
        case n => n
      }
    }
    new RuleTransformer(verbBody).transform(fields)
  }

  /**
   * Check if a message is a delete based on the header status
   *
   * @param message the full oai record with header
   * @return true/false
   */
  def isDelete(message: String): Boolean = {
    val header = XML.loadString(message) \ "header"
    val status = header \ "@status"
    if(status.toString() == "deleted") {
      true
    } else {
      false
    }
  }

  /**
   * Check if a merged record should be deleted in solr
   * This means that all records from the clustered document
   * have received a delete message
   * We check this based on the absence of a 986 marc fiels
   *
   * @param message the merged record (after deduplication)
   * @return true/false
   */
  def isFullDelete(message: String): Boolean = {
    val subfields986 = getSubfields(extractMarcRecord(message), "986", "a")
    subfields986.isEmpty
  }

  /**
   * Get the prefixed deleted id
   * @param message a marc record in an oai envelope
   * @return something like (41SLSP_RZS)996293220105505
   */
  def getPrefixedDeletedId(message: String): String = {
    val identiferFromOaiHeader = (XML.loadString(message) \ "header" \ "identifier").head.text
    val prefix = getInstitutionPrefixFromOaiKey(identiferFromOaiHeader)
    val id = getInstitutionIdFromOaiKey(identiferFromOaiHeader)
    "(" + prefix + ")" + id
  }

}
