/*
 * slsp-deduplication
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import testutils.Helpers._
import utils.MarcExtractorHelpers._

import scala.collection.mutable
import scala.io.Source
import scala.xml.{NodeSeq, XML}

import org.scalatest.StreamlinedXmlEquality._

class MarcExtractorHelpersTest extends AnyFunSuite with Matchers {


  private lazy val marcfileText = getTextFromResourcesFolder("records/9972427470805504.xml")
  private lazy val marcfileXML = getXMLStructureFromResourcesFolder("records/9972427470805504.xml")
  private lazy val marcfileXMLRecordOnly = getXMLStructureFromResourcesFolder("records/9972427470805504-record-only.xml")
  private lazy val duplicateInstance1 = getTextFromResourcesFolder("records/instance1.xml")
  private lazy val duplicateInstance1Updated = getTextFromResourcesFolder("records/instance1updated.xml")
  private lazy val duplicateInstance2 = getTextFromResourcesFolder("records/instance2.xml")
  private lazy val deleteInstance1 = getTextFromResourcesFolder("records/deleteInstance1.xml")
  private lazy val deleteInstance2 = getTextFromResourcesFolder("records/deleteInstance2.xml")

  private def getTextFromResourcesFolder(filepath: String): String = {
    val file = Source.fromFile(s"src/test/resources/$filepath")
    val text = file.mkString
    file.close()
    text
  }

  private def getXMLStructureFromResourcesFolder(filepath: String): NodeSeq = {
    val file = Source.fromFile(s"src/test/resources/$filepath")
    val text = file.mkString
    file.close()
    val result: NodeSeq = XML.loadString(text)
    result
  }

  def makeXmlPretty(document: String): String = {

    val p = new scala.xml.PrettyPrinter(80, 4, true)
    val sB = new mutable.StringBuilder
    p.format(XML.loadString(document), sB)
    sB.toString()
  }

  test ("has net zone id") {
    assert(hasSLSPNetZoneId(marcfileText))

  }

  test("load marc record") {
    val record = extractMarcRecord(marcfileText)
    println(record)
    assert(record.toString.contains("9972427470805504"))
  }

  test ("Replace 001 by NZ Id") {
    val field001before = get001Field(marcfileXMLRecordOnly)
    assert(field001before != "991170497303705501")
    val result = replace001FieldByNzId(marcfileXMLRecordOnly)
    val field001after = get001Field(result)
    assert(field001after == "991170497303705501")

  }

  test ("Add IZ id from source to field") {
    val field852 = getFields(marcfileXMLRecordOnly,"852")
    val enrichedField852 = addIzSourceToMarcField(field852, "22222")
    assert(enrichedField852.toString().contains("<subfield code=\"9\">22222</subfield></datafield>"))
  }

  test ("get 001 Field") {
    assert(get001Field(marcfileXMLRecordOnly) ==
      "9972427470805504")

  }

  test ("get network zone Id") {
    assert(getNetworkZoneIdFromMessage("", marcfileText) ==
      "(EXLNZ-41SLSP_NETWORK)991170497303705501")
  }

  test ("add value as 035") {
    val result = add035and986WithInstitutionPrefixAndIdentifier("oai:alma.41SLSP_UBS:9972427470805504", marcfileXMLRecordOnly)
    val field035 = getSubfields(result, "035", "a").text
    assert(field035.toString.contains("9972427470805504"))
    assert(!field035.toString.contains("other"))
  }

  test ("compare fields") {
    val elem1 = <datafield tag="852" ind1="4" ind2=" ">
      <subfield code="b">A100</subfield>
    </datafield>

    val elem2 = <datafield tag="853" ind1="4" ind2=" ">
      <subfield code="b">A100</subfield>
    </datafield>

    assert(sortFieldsBasedOnTag(elem1, elem2) == true)
    assert(sortFieldsBasedOnTag(elem2, elem1) == false)
  }

  test ("sort fields") {
    val record = getTextFromResourcesFolder("records/record-to-sort.xml")
    val recordAsXML: NodeSeq = XML.loadString(record)
    val result = sortMarcRecord(recordAsXML).toString()
    println(result)
    assert(result.indexOf("SzZuIDS BS/BE A100")<result.indexOf("Musik-Schule des Cantons"))
    assert(result.indexOf("A150testlio")<result.indexOf("Musik-Schule des Cantons"))
  }


  test ("is local field") {
    val elem1 = <datafield tag="852" ind1="4" ind2=" ">
      <subfield code="b">A100</subfield>
      <subfield code="c">102HSS</subfield>
      <subfield code="j">UBH NL 320 : F:VII:b</subfield>
      <subfield code="n">CH</subfield>
      <subfield code="8">22354309610005504</subfield>
      <subfield code="9">(41SLSP_ZBS)9972338884305528</subfield>
    </datafield>

    assert(isLocalFieldFromIZ("(41SLSP_ZBS)9972338884305528", elem1) == true)
    assert(isLocalFieldFromIZ("(41SLSP_ZBS)9972338884305529", elem1) == false)
    assert(isLocalField(elem1) == true)

    val elem_with_multiple_9 = <datafield tag="852" ind1="4" ind2=" ">
      <subfield code="b">A100</subfield>
      <subfield code="c">102HSS</subfield>
      <subfield code="j">UBH NL 320 : F:VII:b</subfield>
      <subfield code="n">CH</subfield>
      <subfield code="8">22354309610005504</subfield>
      <subfield code="9">(41SLSP_ZBS)9972338884305528</subfield>
      <subfield code="9">(41SLSP_ZBS)9972338884305528</subfield>
    </datafield>

    assert(isLocalFieldFromIZ("(41SLSP_ZBS)9972338884305528", elem_with_multiple_9) == true)
    assert(isLocalFieldFromIZ("(41SLSP_ZBS)9972338884305529", elem_with_multiple_9) == false)
    assert(isLocalField(elem_with_multiple_9) == true)

    val elem_without_9 = <datafield tag="852" ind1="4" ind2=" ">
      <subfield code="b">A100</subfield>
      <subfield code="c">102HSS</subfield>
      <subfield code="j">UBH NL 320 : F:VII:b</subfield>
      <subfield code="n">CH</subfield>
      <subfield code="8">22354309610005504</subfield>
    </datafield>

    assert(isLocalFieldFromIZ("(41SLSP_ZBS)9972338884305528", elem_without_9) == false)
    assert(isLocalField(elem_without_9) == false)
  }


  test ("is local field based on $9 LOCAL") {
    val elem1 = <datafield tag="909" ind1=" " ind2=" ">
      <subfield code="a">whatever</subfield>
      <subfield code="9">LOCAL</subfield>
    </datafield>

    assert(is9XXLOCALField(elem1) == true)

    val elem2 = <datafield tag="909" ind1=" " ind2=" ">
      <subfield code="a">whatever</subfield>
      <subfield code="999">LOCAL</subfield>
    </datafield>

    assert(is9XXLOCALField(elem2) == false)

    val elem3 = <datafield tag="909" ind1=" " ind2=" ">
      <subfield code="a">whatever</subfield>
      <subfield code="9">local</subfield>
    </datafield>

    assert(is9XXLOCALField(elem3) == true)

    val elem3b = <datafield tag="909" ind1=" " ind2=" ">
      <subfield code="a">whatever</subfield>
      <subfield code="9">localito</subfield>
    </datafield>

    assert(is9XXLOCALField(elem3b) == false)

    val elem4 = <datafield tag="809" ind1=" " ind2=" ">
      <subfield code="a">whatever</subfield>
      <subfield code="9">LOCAL</subfield>
    </datafield>

    assert(is9XXLOCALField(elem4) == false)


  }

  test ("remove local fields") {
    val record = getXMLStructureFromResourcesFolder("records/record-with-local-fields-marked.xml")
    val result = removeLocalFieldsFromIZ("(41SLSP_ZBS)9972338884305528", record)
    assert(!result.toString.contains("UBH NL 320 : F:VII:b"))
    assert(!result.toString.contains("UBH NL 320 : F:VII:b"))
    assert(result.toString.contains("This call number should remain"))
  }

  test ("extract local fields") {
    val record = getXMLStructureFromResourcesFolder("records/record-with-local-fields-marked.xml")
    val result = extractLocalFieldsFromIZ("(41SLSP_ZBS)9972338884305528", record)
    //println(result)
    assert(result.toString.contains("UBH NL 320 : F:VII:b"))
    assert(result.toString.contains("UBH NL 320 : F:VII:b"))
    assert(!result.toString.contains("This call number should remain"))
  }

  test ("extract 9XX$9LOCAL fields") {
    val result = get9XXLOCALFields(marcfileXMLRecordOnly)
    assert(result.toString().contains("E07-20140110"))
    assert(result.toString().contains("LOCAL"))
    assert(result.toString().contains("990"))
    assert(!result.toString().contains("245"))
  }

  test ("get institution prefix") {
    assert(getInstitutionPrefixFromOaiKey("(UBS)oai:alma.41SLSP_UBS:9972433290505504") ==
      "41SLSP_UBS")
  }

  test ("merge prefixed oai id into doc strcture") {
    val enrichedStructure = add035and986WithInstitutionPrefixAndIdentifier(
      "(UBS)oai:alma.41SLSP_UBS:9972433290505504",
      marcfileXMLRecordOnly
    )
    assert(enrichedStructure.toString.contains("(41SLSP_UBS)9972427470805504"))
  }


  test ("preprocess records") {
    val preparedDoc1 = preProcessRecord(
      "oai:alma.41SLSP_ZBS:9972338884305528", duplicateInstance1)
    //println(makeXmlPretty(preparedDoc1))
    assert(XML.loadString(preparedDoc1) === XML.loadString(getTextFromResourcesFolder("records/result-preprocess.xml")))
  }

  test ("remove namespaces") {
    val record = marcfileXML
    val recordNoNS = removeNamespaces(record.head)
    assert(!recordNoNS.toString().contains("http://www.openarchives.org/OAI/2.0/"))
    assert(recordNoNS.toString().contains("9972427470805504"))
  }


  test ("complete merge example") {
    val preparedDoc1 = preProcessRecord(
      "oai:alma.41SLSP_ZBS:9972338884305528", duplicateInstance1)
    val preparedDoc2 = preProcessRecord(
      "oai:alma.41SLSP_RZS:996293220105505", duplicateInstance2)
    val completeMergedDoc = mergeDocuments(
      preparedDoc1,
      preparedDoc2)


    //println(makeXmlPretty(completeMergedDoc))

    assert(XML.loadString(completeMergedDoc) === XML.loadString(getTextFromResourcesFolder("records/result.xml")))
    assert(completeMergedDoc.contains("<controlfield tag=\"001\">991118884839705501</controlfield>"))
  }

  test ("merge a record, with the same record updated") {
    val preparedDoc1 = preProcessRecord(
      "oai:alma.41SLSP_ZBS:9972338884305528", duplicateInstance1)
    val preparedDoc2 = preProcessRecord(
      "oai:alma.41SLSP_ZBS:9972338884305528", duplicateInstance1Updated)

    val completeMergedDoc = mergeDocuments(
      preparedDoc1,
      preparedDoc2)

    //println(makeXmlPretty(completeMergedDoc))
    assert(XML.loadString(completeMergedDoc) === XML.loadString(getTextFromResourcesFolder("records/result2.xml")))
  }

  test("new record and then delete") {
    val preparedDoc1 = preProcessRecord(
      "oai:alma.41SLSP_ZBS:9972338884305528", duplicateInstance1)
    val deletedDoc = preProcessRecord(
      "oai:alma.41SLSP_ZBS:9972338884305528", deleteInstance1)
    val completeMergedDoc = mergeDocuments(
      preparedDoc1,
      deletedDoc)


    //println(makeXmlPretty(XML.loadString(completeMergedDoc).toString()))
    assert ( XML.loadString(completeMergedDoc) === XML.loadString(getTextFromResourcesFolder("records/result3.xml")))
    assert (isFullDelete(completeMergedDoc))
  }

  test("new record, new record in cluster then delete record 1, then delete record 2") {
    val preparedDoc1 = preProcessRecord(
      "oai:alma.41SLSP_ZBS:9972338884305528", duplicateInstance1)
    val preparedDoc2 = preProcessRecord(
      "oai:alma.41SLSP_RZS:996293220105505", duplicateInstance2)
    val mergedDoc1 = mergeDocuments(
      preparedDoc1,
      preparedDoc2)

    val deletedDoc1 = preProcessRecord(
      "oai:alma.41SLSP_ZBS:9972338884305528", deleteInstance1)

    val mergedDoc2 = mergeDocuments(
      mergedDoc1,
      deletedDoc1
    )

    //println(makeXmlPretty(XML.loadString(mergedDoc2).toString()))

    assert(XML.loadString(mergedDoc2) === XML.loadString(getTextFromResourcesFolder("records/result4.xml")))
    assert(!isFullDelete(mergedDoc2))


    val deletedDoc2 = preProcessRecord(
      "oai:alma.41SLSP_ZBS:9972338884305528", deleteInstance2)
    val mergedDoc3 = mergeDocuments(
      mergedDoc2,
      deletedDoc2
    )

    assert(XML.loadString(mergedDoc3) === XML.loadString(getTextFromResourcesFolder("records/result5.xml")))
    assert(isFullDelete(mergedDoc3))
 }

  test("is full delete") {
    assert(isFullDelete(marcfileText)) //no 986 field
    assert(!isFullDelete(getTextFromResourcesFolder("records/result.xml"))) //multiple 986 fields
    assert(!isFullDelete(getTextFromResourcesFolder("records/result2.xml"))) //one 986 field
  }

  test("is delete") {
    assert(!isDelete(marcfileText)) //no 986 field
    assert(isDelete(deleteInstance1)) //some 986 fields
  }

  test("get deleted id"){
    val id = getPrefixedDeletedId(deleteInstance1)
    assert(id == "(41SLSP_ZBS)9972338884305528")
  }
}


